//
//  DataType.swift
//  swift-code
//
//  Created by ANDRES on 7/08/20.
//  Copyright © 2020 andresduran0205. All rights reserved.
//
import Foundation
import SwiftUI

//------------------------------ GENERIC ------------------------------

extension Decodable {
    public init() {
        self.init()
    }
}

public class Utils: Decodable {
    
    public func convert<T : Decodable, Key>(builder: KeyedDecodingContainer<Key>, type: T.Type, forKey tag: KeyedDecodingContainer<Key>.Key) -> T? {
        
        do {
            return try builder.decodeIfPresent(type, forKey: tag)
        } catch { /*print("=> \(error)")*/ }
        
        if T.self == Bool.self {
            guard let data = try? builder.decodeIfPresent(Bool.self, forKey: tag) else { return false as! T }
            return data as! T
        }
        
        if T.self == String.self {
            guard let data = try? builder.decodeIfPresent(String.self, forKey: tag) else { return "" as! T }
            return data as! T
        }
        
        if T.self == Double.self {
            guard let data = try? builder.decodeIfPresent(Double.self, forKey: tag) else { return 0.0 as! T }
            return data as! T
        }
        
        if T.self == Int.self {
            guard let data = try? builder.decodeIfPresent(Int.self, forKey: tag) else { return 0 as! T }
            return data as! T
        }
        
        return T.init()
    }
    
}

public class Response<T : Decodable> : Identifiable, Decodable {
    public var success: Bool = true
    public var message: String = ""
    public var data: T?
    init(_ object: T) {
        self.data = object
    }
    init(success: Bool, message: String) {
        self.success = success
        self.message = message
    }
}

//------------------------------ END GENERIC ------------------------------

public class Delivery : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case open,closed,online,byphone
    }
    
    public var open: String?
    public var closed: String?
    public var online: String?
    public var byphone: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.open = super.convert(builder: container, type: String.self, forKey: .open)
        self.closed = super.convert(builder: container, type: String.self, forKey: .closed)
        self.online = super.convert(builder: container, type: String.self, forKey: .online)
        self.byphone = super.convert(builder: container, type: String.self, forKey: .byphone)
        
    }
    
    static public func == (lhs: Delivery, rhs: Delivery) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class Offer : Utils, Identifiable,  Hashable{
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case offer,info,normalPrice
    }
    public var offer: String?
    public var info: String?
    public var normalPrice: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.offer = super.convert(builder: container, type: String.self, forKey: .offer)
        self.info = super.convert(builder: container, type: String.self, forKey: .info)
        self.normalPrice = super.convert(builder: container, type: String.self, forKey: .normalPrice)
        
    }
    
    static public func == (lhs: Offer, rhs: Offer) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class FilterItem : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case name,value,count
    }
    public var name: String?
    public var value: String?
    public var count: Int?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = super.convert(builder: container, type: String.self, forKey: .name)
        self.value = super.convert(builder: container, type: String.self, forKey: .value)
        self.count = super.convert(builder: container, type: Int.self, forKey: .count)
        
    }
    
    static public func == (lhs: FilterItem, rhs: FilterItem) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class SubcategorieItem : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case name,value,selected
    }
    public var name: String?
    public var value: String?
    public var selected: Bool?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = super.convert(builder: container, type: String.self, forKey: .name)
        self.value = super.convert(builder: container, type: String.self, forKey: .value)
        self.selected = super.convert(builder: container, type: Bool.self, forKey: .selected)
        
    }
    
    static public func == (lhs: SubcategorieItem, rhs: SubcategorieItem) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class FacetItem : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case name,slug,value,icon,count,selected,subcategories
    }
    public var name: String?
    public var slug: String?
    public var value: String?
    public var icon: String?
    public var count: Int?
    public var selected: Bool?
    public var subcategories: [SubcategorieItem]?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = super.convert(builder: container, type: String.self, forKey: .name)
        self.slug = super.convert(builder: container, type: String.self, forKey: .slug)
        self.value = super.convert(builder: container, type: String.self, forKey: .value)
        self.icon = super.convert(builder: container, type: String.self, forKey: .icon)
        self.count = super.convert(builder: container, type: Int.self, forKey: .count)
        self.selected = super.convert(builder: container, type: Bool.self, forKey: .selected)
        self.subcategories = super.convert(builder: container, type: [SubcategorieItem].self, forKey: .subcategories)
        
    }
    
    static public func == (lhs: FacetItem, rhs: FacetItem) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class FeatureItem : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case name,slug,features
    }
    public var name: String?
    public var slug: String?
    public var features: [Item]?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = super.convert(builder: container, type: String.self, forKey: .name)
        self.slug = super.convert(builder: container, type: String.self, forKey: .slug)
        self.features = super.convert(builder: container, type: [Item].self, forKey: .features)
        
    }
    
    static public func == (lhs: FeatureItem, rhs: FeatureItem) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class Item : Utils, Identifiable, Hashable {
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case value,name,count,selected
        }
        public var value: String?
        public var name: String?
        public var count: Int?
        public var selected: Bool?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.value = super.convert(builder: container, type: String.self, forKey: .value)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
            self.count = super.convert(builder: container, type: Int.self, forKey: .count)
            self.selected = super.convert(builder: container, type: Bool.self, forKey: .selected)
            
        }
        
        static public func == (lhs: Item, rhs: Item) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
}

public class Coordinates : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case lat,long,latitude,longitude
    }
    public var lat: Double?
    public var long: Double?
    public var latitude: Double?
    public var longitude: Double?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lat = super.convert(builder: container, type: Double.self, forKey: .lat)
        self.long = super.convert(builder: container, type: Double.self, forKey: .long)
        self.latitude = super.convert(builder: container, type: Double.self, forKey: .latitude)
        self.longitude = super.convert(builder: container, type: Double.self, forKey: .longitude)
        
    }
    
    static public func == (lhs: Coordinates, rhs: Coordinates) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class SocialMedia : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case _id,facebook,google,instagram,linkedin,modifier_id,pinterest,twitter,version,youtube
    }
    public var _id: String?
    public var facebook: String?
    public var google: String?
    public var instagram: String?
    public var linkedin: String?
    public var modifier_id: String?
    public var pinterest: String?
    public var twitter: String?
    public var version: String?
    public var youtube: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self._id = super.convert(builder: container, type: String.self, forKey: ._id)
        self.facebook = super.convert(builder: container, type: String.self, forKey: .facebook)
        self.google = super.convert(builder: container, type: String.self, forKey: .google)
        self.instagram = super.convert(builder: container, type: String.self, forKey: .instagram)
        self.linkedin = super.convert(builder: container, type: String.self, forKey: .linkedin)
        self.modifier_id = super.convert(builder: container, type: String.self, forKey: .modifier_id)
        self.pinterest = super.convert(builder: container, type: String.self, forKey: .pinterest)
        self.twitter = super.convert(builder: container, type: String.self, forKey: .twitter)
        self.version = super.convert(builder: container, type: String.self, forKey: .version)
        self.youtube = super.convert(builder: container, type: String.self, forKey: .youtube)
        
    }
    
    static public func == (lhs: SocialMedia, rhs: SocialMedia) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class Place : Utils, Identifiable, Hashable {
    
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case id_response = "id",card_type,name,description,address,images,schedules
    }
    public var id_response: String?
    public var card_type: String?
    public var name: String?
    public var description: String?
    public var address: String?
    public var images: [String]?
    public var schedules: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
        self.card_type = super.convert(builder: container, type: String.self, forKey: .card_type)
        self.name = super.convert(builder: container, type: String.self, forKey: .name)
        self.description = super.convert(builder: container, type: String.self, forKey: .description)
        self.address = super.convert(builder: container, type: String.self, forKey: .address)
        self.images = super.convert(builder: container, type: [String].self, forKey: .images)
        self.schedules = super.convert(builder: container, type: String.self, forKey: .schedules)
        
    }
    
    static public func == (lhs: Place, rhs: Place) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class PlaceBranding : Utils, Identifiable, Hashable {
        
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case id_response,name = "id",prefix,image,category,feature,template,gradient_color_start,gradient_color_final,tooltip_color,show_map,map_id,branding_feature,event_start_date,event_end_date,header_background_image,header_background_image_app,images,text_color
        }
        public var id_response: String?
        public var name: String?
        public var prefix: String?
        public var image: String?
        public var category: String?
        public var feature: String?
        public var template: String?
        public var gradient_color_start: String?
        public var gradient_color_final: String?
        public var tooltip_color: String?
        public var show_map: Bool?
        public var map_id: String?
        public var branding_feature: PlaceBrandingFeature?
        public var event_start_date: String?
        public var event_end_date: String?
        public var header_background_image: String?
        public var header_background_image_app: String?
        public var images: PlaceBrandingImages?
        public var text_color: String?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
            self.prefix = super.convert(builder: container, type: String.self, forKey: .prefix)
            self.image = super.convert(builder: container, type: String.self, forKey: .image)
            self.category = super.convert(builder: container, type: String.self, forKey: .category)
            self.feature = super.convert(builder: container, type: String.self, forKey: .feature)
            self.template = super.convert(builder: container, type: String.self, forKey: .template)
            self.gradient_color_start = super.convert(builder: container, type: String.self, forKey: .gradient_color_start)
            self.gradient_color_final = super.convert(builder: container, type: String.self, forKey: .gradient_color_final)
            self.tooltip_color = super.convert(builder: container, type: String.self, forKey: .tooltip_color)
            self.show_map = super.convert(builder: container, type: Bool.self, forKey: .show_map)
            self.map_id = super.convert(builder: container, type: String.self, forKey: .map_id)
            self.branding_feature = super.convert(builder: container, type: PlaceBrandingFeature.self, forKey: .branding_feature)
            self.event_start_date = super.convert(builder: container, type: String.self, forKey: .event_start_date)
            self.event_end_date = super.convert(builder: container, type: String.self, forKey: .event_end_date)
            self.header_background_image = super.convert(builder: container, type: String.self, forKey: .header_background_image)
            self.header_background_image_app = super.convert(builder: container, type: String.self, forKey: .header_background_image_app)
            self.images = super.convert(builder: container, type: PlaceBrandingImages.self, forKey: .images)
            self.text_color = super.convert(builder: container, type: String.self, forKey: .text_color)
            
        }
        
        static public func == (lhs: PlaceBranding, rhs: PlaceBranding) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
        
        public class PlaceBrandingFeature : Utils, Identifiable, Hashable {
            
            public let id: UUID = UUID()
            enum CodingKeys: String, CodingKey, Decodable {
                case id_response = "id",name,logo_image,map_image,search_image,search_text
            }
            public var id_response: String?
            public var name: String?
            public var logo_image: String?
            public var map_image: String?
            public var search_image: String?
            public var search_text: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
                self.name = super.convert(builder: container, type: String.self, forKey: .name)
                self.logo_image = super.convert(builder: container, type: String.self, forKey: .logo_image)
                self.map_image = super.convert(builder: container, type: String.self, forKey: .map_image)
                self.search_image = super.convert(builder: container, type: String.self, forKey: .search_image)
                self.search_text = super.convert(builder: container, type: String.self, forKey: .search_text)
                
            }
            
            static public func == (lhs: PlaceBrandingFeature, rhs: PlaceBrandingFeature) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
        }
        
        public class PlaceBrandingImages : Utils, Identifiable, Hashable {
            public let id: UUID = UUID()
            enum CodingKeys: String, CodingKey, Decodable {
                case header,header_card,civico,responsive,app_banner_image,logo_ldpi,logo_mdpi,logo_hdpi,logo_xhdpi,logo_xxhdpi,header_ldpi,header_mdpi,header_hdpi,header_xhdpi,header_xxhdpi,logo_ios_medium,logo_ios_high,header_ios_medium,header_ios_high
            }
            public var header: String?
            public var header_card: String?
            public var civico: String?
            public var responsive: String?
            public var app_banner_image: String?
            public var logo_ldpi: String?
            public var logo_mdpi: String?
            public var logo_hdpi: String?
            public var logo_xhdpi: String?
            public var logo_xxhdpi: String?
            public var header_ldpi: String?
            public var header_mdpi: String?
            public var header_hdpi: String?
            public var header_xhdpi: String?
            public var header_xxhdpi: String?
            public var logo_ios_medium: String?
            public var logo_ios_high: String?
            public var header_ios_medium: String?
            public var header_ios_high: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.header = super.convert(builder: container, type: String.self, forKey: .header)
                self.header_card = super.convert(builder: container, type: String.self, forKey: .header_card)
                self.civico = super.convert(builder: container, type: String.self, forKey: .civico)
                self.responsive = super.convert(builder: container, type: String.self, forKey: .responsive)
                self.app_banner_image = super.convert(builder: container, type: String.self, forKey: .app_banner_image)
                self.logo_ldpi = super.convert(builder: container, type: String.self, forKey: .logo_ldpi)
                self.logo_mdpi = super.convert(builder: container, type: String.self, forKey: .logo_mdpi)
                self.logo_hdpi = super.convert(builder: container, type: String.self, forKey: .logo_hdpi)
                self.logo_xhdpi = super.convert(builder: container, type: String.self, forKey: .logo_xhdpi)
                self.logo_xxhdpi = super.convert(builder: container, type: String.self, forKey: .logo_xxhdpi)
                self.header_ldpi = super.convert(builder: container, type: String.self, forKey: .header_ldpi)
                self.header_mdpi = super.convert(builder: container, type: String.self, forKey: .header_mdpi)
                self.header_hdpi = super.convert(builder: container, type: String.self, forKey: .header_hdpi)
                self.header_xhdpi = super.convert(builder: container, type: String.self, forKey: .header_xhdpi)
                self.header_xxhdpi = super.convert(builder: container, type: String.self, forKey: .header_xxhdpi)
                self.logo_ios_medium = super.convert(builder: container, type: String.self, forKey: .logo_ios_medium)
                self.logo_ios_high = super.convert(builder: container, type: String.self, forKey: .logo_ios_high)
                self.header_ios_medium = super.convert(builder: container, type: String.self, forKey: .header_ios_medium)
                self.header_ios_high = super.convert(builder: container, type: String.self, forKey: .header_ios_high)
                
            }
            
            static public func == (lhs: PlaceBrandingImages, rhs: PlaceBrandingImages) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
        }
    }
}

public class Picture : Utils, Identifiable, Hashable {
    
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case id_response = "id",name,description,status,author,via,image_alternatives,header,header_card,civico,responsive,app_banner_image,logo_ldpi,logo_mdpi,logo_hdpi,logo_xhdpi,logo_xxhdpi,header_ldpi,header_mdpi,header_hdpi,header_xhdpi,header_xxhdpi,logo_ios_medium,logo_ios_high,header_ios_medium,header_ios_high
    }
    public var id_response: String?
    public var name: String?
    public var description: String?
    public var status: Int?
    public var author: String?
    public var via: String?
    public var image_alternatives: ImageAlternativesItem?
    public var header: String?
    public var header_card: String?
    public var civico: String?
    public var responsive: String?
    public var app_banner_image: String?
    public var logo_ldpi: String?
    public var logo_mdpi: String?
    public var logo_hdpi: String?
    public var logo_xhdpi: String?
    public var logo_xxhdpi: String?
    public var header_ldpi: String?
    public var header_mdpi: String?
    public var header_hdpi: String?
    public var header_xhdpi: String?
    public var header_xxhdpi: String?
    public var logo_ios_medium: String?
    public var logo_ios_high: String?
    public var header_ios_medium: String?
    public var header_ios_high: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
        self.name = super.convert(builder: container, type: String.self, forKey: .name)
        self.description = super.convert(builder: container, type: String.self, forKey: .description)
        self.status = super.convert(builder: container, type: Int.self, forKey: .status)
        self.author = super.convert(builder: container, type: String.self, forKey: .author)
        self.via = super.convert(builder: container, type: String.self, forKey: .via)
        self.image_alternatives = super.convert(builder: container, type: ImageAlternativesItem.self, forKey: .image_alternatives)
        self.header = super.convert(builder: container, type: String.self, forKey: .header)
        self.header_card = super.convert(builder: container, type: String.self, forKey: .header_card)
        self.civico = super.convert(builder: container, type: String.self, forKey: .civico)
        self.responsive = super.convert(builder: container, type: String.self, forKey: .responsive)
        self.app_banner_image = super.convert(builder: container, type: String.self, forKey: .app_banner_image)
        self.logo_ldpi = super.convert(builder: container, type: String.self, forKey: .logo_ldpi)
        self.logo_mdpi = super.convert(builder: container, type: String.self, forKey: .logo_mdpi)
        self.logo_hdpi = super.convert(builder: container, type: String.self, forKey: .logo_hdpi)
        self.logo_xhdpi = super.convert(builder: container, type: String.self, forKey: .logo_xhdpi)
        self.logo_xxhdpi = super.convert(builder: container, type: String.self, forKey: .logo_xxhdpi)
        self.header_ldpi = super.convert(builder: container, type: String.self, forKey: .header_ldpi)
        self.header_mdpi = super.convert(builder: container, type: String.self, forKey: .header_mdpi)
        self.header_hdpi = super.convert(builder: container, type: String.self, forKey: .header_hdpi)
        self.header_xhdpi = super.convert(builder: container, type: String.self, forKey: .header_xhdpi)
        self.header_xxhdpi = super.convert(builder: container, type: String.self, forKey: .header_xxhdpi)
        self.logo_ios_medium = super.convert(builder: container, type: String.self, forKey: .logo_ios_medium)
        self.logo_ios_high = super.convert(builder: container, type: String.self, forKey: .logo_ios_high)
        self.header_ios_medium = super.convert(builder: container, type: String.self, forKey: .header_ios_medium)
        self.header_ios_high = super.convert(builder: container, type: String.self, forKey: .header_ios_high)
    }
    
    static public func == (lhs: Picture, rhs: Picture) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class FirstSecondEIItem : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case first,second,third
    }
    public var first: String?
    public var second: String?
    public var third: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.first = super.convert(builder: container, type: String.self, forKey: .first)
        self.second = super.convert(builder: container, type: String.self, forKey: .second)
        self.third = super.convert(builder: container, type: String.self, forKey: .third)
    }
    
    static public func == (lhs: FirstSecondEIItem, rhs: FirstSecondEIItem) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class ImageAlternativesItem : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case thumb,mobile_card,web_card,highlighted_card
    }
    public var thumb: String?
    public var mobile_card: String?
    public var web_card: String?
    public var highlighted_card: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.thumb = super.convert(builder: container, type: String.self, forKey: .thumb)
        self.mobile_card = super.convert(builder: container, type: String.self, forKey: .mobile_card)
        self.web_card = super.convert(builder: container, type: String.self, forKey: .web_card)
        self.highlighted_card = super.convert(builder: container, type: String.self, forKey: .highlighted_card)
    }
    
    static public func == (lhs: ImageAlternativesItem, rhs: ImageAlternativesItem) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class ScheduleItem : Utils, Identifiable, Hashable {
    
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case id_response = "id",start_day,end_day,start_hour,end_hour
    }
    public var id_response: String?
    public var start_day: Int?
    public var end_day: Int?
    public var start_hour: String?
    public var end_hour: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
        self.start_day = super.convert(builder: container, type: Int.self, forKey: .start_day)
        self.end_day = super.convert(builder: container, type: Int.self, forKey: .end_day)
        self.start_hour = super.convert(builder: container, type: String.self, forKey: .start_hour)
        self.end_hour = super.convert(builder: container, type: String.self, forKey: .end_hour)
    }
    
    static public func == (lhs: ScheduleItem, rhs: ScheduleItem) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class UrlString : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case url
    }
    public var url: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = super.convert(builder: container, type: String.self, forKey: .url)
    }
    
    static public func == (lhs: UrlString, rhs: UrlString) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class StatusResponse : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case status,success,error
    }
    public var status: String?
    public var success: Bool?
    public var error: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.status = super.convert(builder: container, type: String.self, forKey: .status)
        self.success = super.convert(builder: container, type: Bool.self, forKey: .success)
        self.error = super.convert(builder: container, type: String.self, forKey: .error)
    }
    
    
    static public func == (lhs: StatusResponse, rhs: StatusResponse) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

//------------------------------ SIMPLE_COMPLEX ------------------------------

public class ResultItemPhoneNumberDecorated : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case id_response = "id", type, number, number_e164, mobile_dialing
    }
    public var id_response: String?
    public var type: String?
    public var number: String?
    public var number_e164: String?
    public var mobile_dialing: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
        self.type = super.convert(builder: container, type: String.self, forKey: .type)
        self.number = super.convert(builder: container, type: String.self, forKey: .number)
        self.number_e164 = super.convert(builder: container, type: String.self, forKey: .number_e164)
        self.mobile_dialing = super.convert(builder: container, type: String.self, forKey: .mobile_dialing)
    }
    
    static public func == (lhs: ResultItemPhoneNumberDecorated, rhs: ResultItemPhoneNumberDecorated) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class ResultItemBrandIconSearchImage : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case url,stream
    }
    public var url: String?
    public var stream: UrlString?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = super.convert(builder: container, type: String.self, forKey: .url)
        self.stream = super.convert(builder: container, type: UrlString.self, forKey: .stream)
    }
    
    static public func == (lhs: ResultItemBrandIconSearchImage, rhs: ResultItemBrandIconSearchImage) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class ResultItemBrandLogo : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case url,middle,logo,logo_email,missions_logo,bar_section,circular
    }
    public var url: String?
    public var middle: UrlString?
    public var logo: UrlString?
    public var logo_email: UrlString?
    public var missions_logo: UrlString?
    public var bar_section: UrlString?
    public var circular: UrlString?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = super.convert(builder: container, type: String.self, forKey: .url)
        self.middle = super.convert(builder: container, type: UrlString.self, forKey: .middle)
        self.logo = super.convert(builder: container, type: UrlString.self, forKey: .logo)
        self.logo_email = super.convert(builder: container, type: UrlString.self, forKey: .logo_email)
        self.missions_logo = super.convert(builder: container, type: UrlString.self, forKey: .missions_logo)
        self.bar_section = super.convert(builder: container, type: UrlString.self, forKey: .bar_section)
        self.circular = super.convert(builder: container, type: UrlString.self, forKey: .circular)
    }
    
    static public func == (lhs: ResultItemBrandLogo, rhs: ResultItemBrandLogo) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class ResultItemBrandMissionsImage : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case url,header
    }
    public var url: String?
    public var header: UrlString?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = super.convert(builder: container, type: String.self, forKey: .url)
        self.header = super.convert(builder: container, type: UrlString.self, forKey: .header)
    }
    
    static public func == (lhs: ResultItemBrandMissionsImage, rhs: ResultItemBrandMissionsImage) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

public class ResultItemDelivery : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case open,externalFlag,externalUrl,phonesFlag,phones,formattedPhones,extra_info,methods
    }
    public var open: Bool?
    public var externalFlag: Bool?
    public var externalUrl: String?
    public var phonesFlag: Bool?
    public var phones: [String]?
    public var formattedPhones: [String]?
    public var extra_info: String?
    public var methods: [String]?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.open = super.convert(builder: container, type: Bool.self, forKey: .open)
        self.externalFlag = super.convert(builder: container, type: Bool.self, forKey: .externalFlag)
        self.externalUrl = super.convert(builder: container, type: String.self, forKey: .externalUrl)
        self.phonesFlag = super.convert(builder: container, type: Bool.self, forKey: .phonesFlag)
        self.phones = super.convert(builder: container, type: [String].self, forKey: .phones)
        self.formattedPhones = super.convert(builder: container, type: [String].self, forKey: .formattedPhones)
        self.extra_info = super.convert(builder: container, type: String.self, forKey: .extra_info)
        self.methods = super.convert(builder: container, type: [String].self, forKey: .methods)
    }
    
    static public func == (lhs: ResultItemDelivery, rhs: ResultItemDelivery) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}



public class ResultItemBrand : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case _id,_slugs,blocked,city_id,created_at,customer_id,description,email,html_description,icon_search_image,logo,major_beacon_id,minor_beacon_id,mission_description,mission_title,missions_image,modifier_id,name,owner_id,premium,premium_tpl,social,squared_file,updated_at,valid,version,wp_author_id
    }
    public var _id: String?
    public var _slugs: [String]?
    public var blocked: Bool?
    public var city_id: String?
    public var created_at: String?
    public var customer_id: String?
    public var description: String?
    public var email: String?
    public var html_description: String?
    public var icon_search_image: ResultItemBrandIconSearchImage?
    public var logo: ResultItemBrandLogo?
    public var major_beacon_id: String?
    public var minor_beacon_id: Int?
    public var mission_description: String?
    public var mission_title: String?
    public var missions_image: ResultItemBrandMissionsImage?
    public var modifier_id: String?
    public var name: String?
    public var owner_id: String?
    public var premium: Bool?
    public var premium_tpl: Int?
    public var social: SocialMedia?
    public var squared_file: UrlString?
    public var updated_at: String?
    public var valid: Bool?
    public var version: Int?
    public var wp_author_id: String?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self._id = super.convert(builder: container, type: String.self, forKey: ._id)
        self._slugs = super.convert(builder: container, type: [String].self, forKey: ._slugs)
        self.blocked = super.convert(builder: container, type: Bool.self, forKey: .blocked)
        self.city_id = super.convert(builder: container, type: String.self, forKey: .city_id)
        self.created_at = super.convert(builder: container, type: String.self, forKey: .created_at)
        self.customer_id = super.convert(builder: container, type: String.self, forKey: .customer_id)
        self.description = super.convert(builder: container, type: String.self, forKey: .description)
        self.email = super.convert(builder: container, type: String.self, forKey: .email)
        self.html_description = super.convert(builder: container, type: String.self, forKey: .html_description)
        self.icon_search_image = super.convert(builder: container, type: ResultItemBrandIconSearchImage.self, forKey: .icon_search_image)
        self.logo = super.convert(builder: container, type: ResultItemBrandLogo.self, forKey: .logo)
        self.major_beacon_id = super.convert(builder: container, type: String.self, forKey: .major_beacon_id)
        self.minor_beacon_id = super.convert(builder: container, type: Int.self, forKey: .minor_beacon_id)
        self.mission_description = super.convert(builder: container, type: String.self, forKey: .mission_description)
        self.mission_title = super.convert(builder: container, type: String.self, forKey: .mission_title)
        self.missions_image = super.convert(builder: container, type: ResultItemBrandMissionsImage.self, forKey: .missions_image)
        self.modifier_id = super.convert(builder: container, type: String.self, forKey: .modifier_id)
        self.name = super.convert(builder: container, type: String.self, forKey: .name)
        self.owner_id = super.convert(builder: container, type: String.self, forKey: .owner_id)
        self.premium = super.convert(builder: container, type: Bool.self, forKey: .premium)
        self.premium_tpl = super.convert(builder: container, type: Int.self, forKey: .premium_tpl)
        self.social = super.convert(builder: container, type: SocialMedia.self, forKey: .social)
        self.squared_file = super.convert(builder: container, type: UrlString.self, forKey: .squared_file)
        self.updated_at = super.convert(builder: container, type: String.self, forKey: .updated_at)
        self.valid = super.convert(builder: container, type: Bool.self, forKey: .valid)
        self.version = super.convert(builder: container, type: Int.self, forKey: .version)
        self.wp_author_id = super.convert(builder: container, type: String.self, forKey: .wp_author_id)
    }
    
    static public func == (lhs: ResultItemBrand, rhs: ResultItemBrand) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}


//------------------------------ COMPLEX FindResponse ------------------------------
public class FindResponse : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    
    enum CodingKeys: String, CodingKey, Decodable {
        case from,copies,error_location,searchTerm,rate,radio,searchedEntities,isOpen,total_entries,total_pages,current_page,filters,did_you_mean,facets,features,featuresCount,neighborhoods,genres,results
    }
    
    public var from: String?
    public var copies: Copie<Delivery,Offer>?
    public var error_location: Bool?
    public var searchTerm: String?
    public var rate: String?
    public var radio: String?
    public var searchedEntities: [String]?
    public var isOpen: Bool?
    public var total_entries: Int?
    public var total_pages: Int?
    public var current_page: String?
    public var filters: [FilterItem]?
    public var did_you_mean: [String]?
    public var facets: [FacetItem]?
    public var features: [FeatureItem]?
    public var featuresCount: Int?
    public var neighborhoods: [String]?
    public var genres: [String]?
    public var results: [ResultItem]?
    
    override public init(){
        super.init()
    }
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.from = super.convert(builder: container, type: String.self, forKey: .from)
        self.copies = super.convert(builder: container, type: Copie<Delivery,Offer>.self, forKey: .copies)
        self.error_location = super.convert(builder: container, type: Bool.self, forKey: .error_location)
        self.searchTerm = super.convert(builder: container, type: String.self, forKey: .searchTerm)
        self.rate = super.convert(builder: container, type: String.self, forKey: .rate)
        self.radio = super.convert(builder: container, type: String.self, forKey: .radio)
        self.searchedEntities = super.convert(builder: container, type: [String].self, forKey: .searchedEntities)
        self.isOpen = super.convert(builder: container, type: Bool.self, forKey: .isOpen)
        self.total_entries = super.convert(builder: container, type: Int.self, forKey: .total_entries)
        self.total_pages = super.convert(builder: container, type: Int.self, forKey: .total_pages)
        self.current_page = super.convert(builder: container, type: String.self, forKey: .current_page)
        self.filters = super.convert(builder: container, type: [FilterItem].self, forKey: .filters)
        self.did_you_mean = super.convert(builder: container, type: [String].self, forKey: .did_you_mean)
        self.facets = super.convert(builder: container, type: [FacetItem].self, forKey: .facets)
        self.features = super.convert(builder: container, type: [FeatureItem].self, forKey: .features)
        self.featuresCount = super.convert(builder: container, type: Int.self, forKey: .featuresCount)
        self.neighborhoods = super.convert(builder: container, type: [String].self, forKey: .neighborhoods)
        self.genres = super.convert(builder: container, type: [String].self, forKey: .genres)
        self.results = super.convert(builder: container, type: [ResultItem].self, forKey: .results)
    }
    
    static public func == (lhs: FindResponse, rhs: FindResponse) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class Copie <A : Decodable,B : Decodable> : Utils, Identifiable, Hashable {
        public let id: UUID = UUID()
        
        enum CodingKeys: String, CodingKey, Decodable {
            case delivery,offer
        }
        
        public var delivery: A?
        public var offer: B?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.delivery = super.convert(builder: container, type: A.self, forKey: .delivery)
            self.offer = super.convert(builder: container, type: B.self, forKey: .offer)
        }
        
        static public func == (lhs: Copie, rhs: Copie) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
    
    public class ResultItem :  Utils, Identifiable, Hashable {
        
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case id_response = "id" ,name,name_html,image,type,description,start_date,end_date,schedule,dateInterval,price,price_formatted,discount,discount_formatted,percentOff,brand_logo,redeem_coupons,avaliable_coupons,coupons_per_user,multioffer,coupons_left,coupon_id,redeemedTimes,date_color,date_icon,obtain_color,redeemed,places,small_business,category,category_slug,main_category,main_category_slug,location,rates_count,address,reviews_count,rate_average,phone_number,phone_number_decorated,url,coordinates,is_theater,movies,premium,branding_icon,has_pay,schedule_search,schedule_simple,is_open,distance
        }
        public var id_response: String?
        public var name: String?
        public var name_html: String?
        public var image: String?
        public var type: String?
        public var description: String?
        public var start_date: String?
        public var end_date: String?
        public var schedule: String?
        public var dateInterval: String?
        public var price: Double?
        public var price_formatted: String?
        public var discount: Int?
        public var discount_formatted: String?
        public var percentOff: String?
        public var brand_logo: String?
        public var redeem_coupons: Int?
        public var avaliable_coupons: Int?
        public var coupons_per_user: Int?
        public var multioffer: Bool?
        public var coupons_left: Int?
        public var coupon_id: String?
        public var redeemedTimes: Int?
        public var date_color: String?
        public var date_icon: String?
        public var obtain_color: String?
        public var redeemed: Bool?
        public var places: [Place]?
        public var small_business: Bool?
        public var category: String?
        public var category_slug: String?
        public var main_category: String?
        public var main_category_slug: String?
        public var location: String?
        public var rates_count: Int?
        public var address: String?
        public var reviews_count: Int?
        public var rate_average: Double?
        public var phone_number: String?
        public var phone_number_decorated: ResultItemPhoneNumberDecorated?
        public var url: String?
        public var coordinates: Coordinates?
        public var is_theater: Bool?
        public var movies: [String]?
        public var premium: Bool?
        public var branding_icon: String?
        public var has_pay: Bool?
        public var schedule_search: String?
        public var schedule_simple: String?
        public var is_open: Bool?
        public var distance: String?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
            self.name_html = super.convert(builder: container, type: String.self, forKey: .name_html)
            self.image = super.convert(builder: container, type: String.self, forKey: .image)
            self.type = super.convert(builder: container, type: String.self, forKey: .type)
            self.description = super.convert(builder: container, type: String.self, forKey: .description)
            self.start_date = super.convert(builder: container, type: String.self, forKey: .start_date)
            self.end_date = super.convert(builder: container, type: String.self, forKey: .end_date)
            self.schedule = super.convert(builder: container, type: String.self, forKey: .schedule)
            self.dateInterval = super.convert(builder: container, type: String.self, forKey: .dateInterval)
            self.price = super.convert(builder: container, type: Double.self, forKey: .price)
            self.price_formatted = super.convert(builder: container, type: String.self, forKey: .price_formatted)
            self.discount = super.convert(builder: container, type: Int.self, forKey: .discount)
            self.discount_formatted = super.convert(builder: container, type: String.self, forKey: .discount_formatted)
            self.percentOff = super.convert(builder: container, type: String.self, forKey: .percentOff)
            self.brand_logo = super.convert(builder: container, type: String.self, forKey: .brand_logo)
            self.redeem_coupons = super.convert(builder: container, type: Int.self, forKey: .redeem_coupons)
            self.avaliable_coupons = super.convert(builder: container, type: Int.self, forKey: .avaliable_coupons)
            self.coupons_per_user = super.convert(builder: container, type: Int.self, forKey: .coupons_per_user)
            self.multioffer = super.convert(builder: container, type: Bool.self, forKey: .multioffer)
            self.coupons_left = super.convert(builder: container, type: Int.self, forKey: .coupons_left)
            self.coupon_id = super.convert(builder: container, type: String.self, forKey: .coupon_id)
            self.redeemedTimes = super.convert(builder: container, type: Int.self, forKey: .redeemedTimes)
            self.date_color = super.convert(builder: container, type: String.self, forKey: .date_color)
            self.date_icon = super.convert(builder: container, type: String.self, forKey: .date_icon)
            self.obtain_color = super.convert(builder: container, type: String.self, forKey: .obtain_color)
            self.redeemed = super.convert(builder: container, type: Bool.self, forKey: .redeemed)
            self.places = super.convert(builder: container, type: [Place].self, forKey: .places)
            self.small_business = super.convert(builder: container, type: Bool.self, forKey: .small_business)
            self.category = super.convert(builder: container, type: String.self, forKey: .category)
            self.category_slug = super.convert(builder: container, type: String.self, forKey: .category_slug)
            self.main_category = super.convert(builder: container, type: String.self, forKey: .main_category)
            self.main_category_slug = super.convert(builder: container, type: String.self, forKey: .main_category_slug)
            self.location = super.convert(builder: container, type: String.self, forKey: .location)
            self.rates_count = super.convert(builder: container, type: Int.self, forKey: .rates_count)
            self.address = super.convert(builder: container, type: String.self, forKey: .address)
            self.reviews_count = super.convert(builder: container, type: Int.self, forKey: .reviews_count)
            self.rate_average = super.convert(builder: container, type: Double.self, forKey: .rate_average)
            self.phone_number = super.convert(builder: container, type: String.self, forKey: .phone_number)
            self.phone_number_decorated = super.convert(builder: container, type: ResultItemPhoneNumberDecorated.self, forKey: .phone_number_decorated)
            self.url = super.convert(builder: container, type: String.self, forKey: .url)
            self.coordinates = super.convert(builder: container, type: Coordinates.self, forKey: .coordinates)
            self.is_theater = super.convert(builder: container, type: Bool.self, forKey: .is_theater)
            self.movies = super.convert(builder: container, type: [String].self, forKey: .movies)
            self.premium = super.convert(builder: container, type: Bool.self, forKey: .premium)
            self.branding_icon = super.convert(builder: container, type: String.self, forKey: .branding_icon)
            self.has_pay = super.convert(builder: container, type: Bool.self, forKey: .has_pay)
            self.schedule_search = super.convert(builder: container, type: String.self, forKey: .schedule_search)
            self.schedule_simple = super.convert(builder: container, type: String.self, forKey: .schedule_simple)
            self.is_open = super.convert(builder: container, type: Bool.self, forKey: .is_open)
            self.distance = super.convert(builder: container, type: String.self, forKey: .distance)
        }
        
        static public func == (lhs: ResultItem, rhs: ResultItem) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
}

//------------------------------ COMPLEX AutocompleteResponse ------------------------------

public class AutocompleteResponse : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case entities,suggestions,results,categories
    }
    public var entities: [String]?
    public var suggestions: [String]?
    public var results: [ResultsItem]?
    public var categories: [CategorieItem]?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.entities = super.convert(builder: container, type: [String].self, forKey: .entities)
        self.suggestions = super.convert(builder: container, type: [String].self, forKey: .suggestions)
        self.results = super.convert(builder: container, type: [ResultsItem].self, forKey: .results)
        self.categories = super.convert(builder: container, type: [CategorieItem].self, forKey: .categories)
    }
    
    static public func == (lhs: AutocompleteResponse, rhs: AutocompleteResponse) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class ResultsItem : Utils, Identifiable, Hashable{
        public let id: UUID = UUID()
        
        enum CodingKeys: String, CodingKey, Decodable {
            case id_response = "id",name,category,type,info
        }
        public var id_response: String?
        public var name: String?
        public var category: String?
        public var type: String?
        public var info: String?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
            self.category = super.convert(builder: container, type: String.self, forKey: .category)
            self.type = super.convert(builder: container, type: String.self, forKey: .type)
            self.info = super.convert(builder: container, type: String.self, forKey: .info)
            
        }
        
        static public func == (lhs: ResultsItem, rhs: ResultsItem) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
    
    public class CategorieItem : Utils, Identifiable, Hashable {
        
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case id_response = "id",name
        }
        public var id_response: String?
        public var name: String?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
        }
        
        static public func == (lhs: CategorieItem, rhs: CategorieItem) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
}
//------------------------------ COMPLEX FindMessageResponse ------------------------------

public class FindMessageResponse : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case message,results,metadata,responses
    }
    public var message: ResultItem?
    public var results: [ResultItem]?
    public var metadata: MetadataItem?
    public var responses: [ResponseItem]?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.message = super.convert(builder: container, type: ResultItem.self, forKey: .message)
        self.results = super.convert(builder: container, type: [ResultItem].self, forKey: .results)
        self.metadata = super.convert(builder: container, type: MetadataItem.self, forKey: .metadata)
        self.responses = super.convert(builder: container, type: [ResponseItem].self, forKey: .responses)
    }
    
    static public func == (lhs: FindMessageResponse, rhs: FindMessageResponse) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class ResultItem : Utils, Identifiable, Hashable {
        
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case id_response = "id" ,place,type,user_name,user_email,user_id,created_at,sender_status,message,total_responses,responses
        }
        public var id_response: String?
        public var place: String?
        public var type: String?
        public var user_name: String?
        public var user_email: String?
        public var user_id: String?
        public var created_at: String?
        public var sender_status: String?
        public var message: String?
        public var total_responses: Int?
        public var responses: [ResponseItem]?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
            self.place = super.convert(builder: container, type: String.self, forKey: .place)
            self.type = super.convert(builder: container, type: String.self, forKey: .type)
            self.user_name = super.convert(builder: container, type: String.self, forKey: .user_name)
            self.user_email = super.convert(builder: container, type: String.self, forKey: .user_email)
            self.user_id = super.convert(builder: container, type: String.self, forKey: .user_id)
            self.created_at = super.convert(builder: container, type: String.self, forKey: .created_at)
            self.sender_status = super.convert(builder: container, type: String.self, forKey: .sender_status)
            self.message = super.convert(builder: container, type: String.self, forKey: .message)
            self.total_responses = super.convert(builder: container, type: Int.self, forKey: .total_responses)
            self.responses = super.convert(builder: container, type: [ResponseItem].self, forKey: .responses)
        }
        
        static public func == (lhs: ResultItem, rhs: ResultItem) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
    
    public class ResponseItem : Utils, Identifiable, Hashable {
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case id_response = "id",created_at,text,user_id
        }
        public var id_response: String?
        public var created_at: String?
        public var text: String?
        public var user_id: String?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
            self.created_at = super.convert(builder: container, type: String.self, forKey: .created_at)
            self.text = super.convert(builder: container, type: String.self, forKey: .text)
            self.user_id = super.convert(builder: container, type: String.self, forKey: .user_id)
            
        }
        
        static public func == (lhs: ResponseItem, rhs: ResponseItem) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
    
    public class MetadataItem : Utils, Identifiable, Hashable {
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case page,limit,total_pages,total_count,total_unread_messages
        }
        public var page: Int?
        public var limit: Int?
        public var total_pages: Int?
        public var total_count: Int?
        public var total_unread_messages: Int?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.page = super.convert(builder: container, type: Int.self, forKey: .page)
            self.limit = super.convert(builder: container, type: Int.self, forKey: .limit)
            self.total_pages = super.convert(builder: container, type: Int.self, forKey: .total_pages)
            self.total_count = super.convert(builder: container, type: Int.self, forKey: .total_count)
            self.total_unread_messages = super.convert(builder: container, type: Int.self, forKey: .total_unread_messages)
        }
        
        static public func == (lhs: MetadataItem, rhs: MetadataItem) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }
}

//------------------------------ COMPLEX FindCategoryResponse ------------------------------

public class FindCategoryResponse : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case categories
    }
    public var categories: [CategorieItem]?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.categories = super.convert(builder: container, type: [CategorieItem].self, forKey: .categories)
    }
    
    static public func == (lhs: FindCategoryResponse, rhs: FindCategoryResponse) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class CategorieItem : Utils, Identifiable, Hashable {
        public let id: UUID = UUID()
        enum CodingKeys: String, CodingKey, Decodable {
            case id_response = "id",name,name_translations,description,image,image_alternatives,parent_id,prefered
        }
        public var id_response: String?
        public var name: String?
        public var name_translations: NameTranslations?
        public var description: String?
        public var image: String?
        public var image_alternatives: ImageAlternatives?
        public var parent_id: String?
        public var prefered: Bool?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id_response = super.convert(builder: container, type: String.self, forKey: .id_response)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
            self.name_translations = super.convert(builder: container, type: NameTranslations.self, forKey: .name_translations)
            self.description = super.convert(builder: container, type: String.self, forKey: .description)
            self.image = super.convert(builder: container, type: String.self, forKey: .image)
            self.image_alternatives = super.convert(builder: container, type: ImageAlternatives.self, forKey: .image_alternatives)
            self.parent_id = super.convert(builder: container, type: String.self, forKey: .parent_id)
            self.prefered = super.convert(builder: container, type: Bool.self, forKey: .prefered)
        }
        
        static public func == (lhs: CategorieItem, rhs: CategorieItem) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
        
        public class NameTranslations : Utils, Identifiable, Hashable {
            public let id: UUID = UUID()
            enum CodingKeys: String, CodingKey, Decodable {
                case bogota,santiago,mexico
            }
            public var bogota: String?
            public var santiago: String?
            public var mexico: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.bogota = super.convert(builder: container, type: String.self, forKey: .bogota)
                self.santiago = super.convert(builder: container, type: String.self, forKey: .santiago)
                self.mexico = super.convert(builder: container, type: String.self, forKey: .mexico)
            }
            
            static public func == (lhs: NameTranslations, rhs: NameTranslations) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
        }
        
        public class ImageAlternatives : Utils, Identifiable, Hashable{
            public let id: UUID = UUID()
            enum CodingKeys: String, CodingKey, Decodable {
                case web_wizard,mobile_wizard,web_recommended
            }
            public var web_wizard: String?
            public var mobile_wizard: String?
            public var web_recommended: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.web_wizard = super.convert(builder: container, type: String.self, forKey: .web_wizard)
                self.mobile_wizard = super.convert(builder: container, type: String.self, forKey: .mobile_wizard)
                self.web_recommended = super.convert(builder: container, type: String.self, forKey: .web_recommended)
                
            }
            
            static public func == (lhs: ImageAlternatives, rhs: ImageAlternatives) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
        }
        
    }
}

//------------------------------ COMPLEX FindCategoryResponse ------------------------------

public class FindChipProductResponse : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case card_data = "card-data"
    }
    
    public var card_data: CardData?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.card_data = super.convert(builder: container, type: CardData.self, forKey: .card_data)
    }
    
    static public func == (lhs: FindChipProductResponse, rhs: FindChipProductResponse) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class CardData : Utils, Identifiable, Hashable {
        enum CodingKeys: String, CodingKey, Decodable {
            case id,card_type,name,display_name,card_name,offer_type,booking_url,images,description,price,discount,image,display_image,card_image,date,date_color,offer_label_color,offer_visits,included,included_before,included_after,conditions,conditions_civico,active,score,lists,uri,where_str,offer_places,brand_name,brand_description,multioffer,brand_logo,only_adult,dateInterval,main_place_name,amount,tax_amount,iac_amount,tip_amount,base_amount,phonenumber,other_offers,redeemedTimes,coupon_id,coupons_left,downloaded,markers,pending_payment
        }
        public var id: String?
        public var card_type: String?
        public var name: String?
        public var display_name: String?
        public var card_name: String?
        public var offer_type: String?
        public var booking_url: String?
        public var images: [Picture]?
        public var description: String?
        public var price: Double?
        public var discount: Double?
        public var image: String?
        public var display_image: String?
        public var card_image: String?
        public var date: String?
        public var date_color: String?
        public var offer_label_color: String?
        public var offer_visits: Int?
        public var included: [String]?
        public var included_before: [String]?
        public var included_after: [String]?
        public var conditions: [String]?
        public var conditions_civico: [String]?
        public var active: Bool?
        public var score: ScoreItem?
        public var lists: [String]?
        public var uri: String?
        public var where_str: [String]?
        public var offer_places: [OfferPlaceItem]?
        public var brand_name: String?
        public var brand_description: String?
        public var multioffer: Bool?
        public var brand_logo: String?
        public var only_adult: Bool?
        public var dateInterval: String?
        public var main_place_name: String?
        public var amount: Int?
        public var tax_amount: Int?
        public var iac_amount: Int?
        public var tip_amount: Int?
        public var base_amount: Int?
        public var phonenumber: String?
        public var other_offers: [OtherOfferItem]?
        public var redeemedTimes: Int?
        public var coupon_id: String?
        public var coupons_left: Int?
        public var downloaded: Bool?
        public var markers: [String]?
        public var pending_payment: [String]?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.id = super.convert(builder: container, type: String.self, forKey: .id)
            self.card_type = super.convert(builder: container, type: String.self, forKey: .card_type)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
            self.display_name = super.convert(builder: container, type: String.self, forKey: .display_name)
            self.card_name = super.convert(builder: container, type: String.self, forKey: .card_name)
            self.offer_type = super.convert(builder: container, type: String.self, forKey: .offer_type)
            self.booking_url = super.convert(builder: container, type: String.self, forKey: .booking_url)
            self.images = super.convert(builder: container, type: [Picture].self, forKey: .images)
            self.description = super.convert(builder: container, type: String.self, forKey: .description)
            self.price = super.convert(builder: container, type: Double.self, forKey: .price)
            self.discount = super.convert(builder: container, type: Double.self, forKey: .discount)
            self.image = super.convert(builder: container, type: String.self, forKey: .image)
            self.display_image = super.convert(builder: container, type: String.self, forKey: .display_image)
            self.card_image = super.convert(builder: container, type: String.self, forKey: .card_image)
            self.date = super.convert(builder: container, type: String.self, forKey: .date)
            self.date_color = super.convert(builder: container, type: String.self, forKey: .date_color)
            self.offer_label_color = super.convert(builder: container, type: String.self, forKey: .offer_label_color)
            self.offer_visits = super.convert(builder: container, type: Int.self, forKey: .offer_visits)
            self.included = super.convert(builder: container, type: [String].self, forKey: .included)
            self.included_before = super.convert(builder: container, type: [String].self, forKey: .included_before)
            self.included_after = super.convert(builder: container, type: [String].self, forKey: .included_after)
            self.conditions = super.convert(builder: container, type: [String].self, forKey: .conditions)
            self.conditions_civico = super.convert(builder: container, type: [String].self, forKey: .conditions_civico)
            self.active = super.convert(builder: container, type: Bool.self, forKey: .active)
            self.score = super.convert(builder: container, type: ScoreItem.self, forKey: .score)
            self.lists = super.convert(builder: container, type: [String].self, forKey: .lists)
            self.uri = super.convert(builder: container, type: String.self, forKey: .uri)
            self.where_str = super.convert(builder: container, type: [String].self, forKey: .where_str)
            self.offer_places = super.convert(builder: container, type: [OfferPlaceItem].self, forKey: .offer_places)
            self.brand_name = super.convert(builder: container, type: String.self, forKey: .brand_name)
            self.brand_description = super.convert(builder: container, type: String.self, forKey: .brand_description)
            self.multioffer = super.convert(builder: container, type: Bool.self, forKey: .multioffer)
            self.brand_logo = super.convert(builder: container, type: String.self, forKey: .brand_logo)
            self.only_adult = super.convert(builder: container, type: Bool.self, forKey: .only_adult)
            self.dateInterval = super.convert(builder: container, type: String.self, forKey: .dateInterval)
            self.main_place_name = super.convert(builder: container, type: String.self, forKey: .main_place_name)
            self.amount = super.convert(builder: container, type: Int.self, forKey: .amount)
            self.tax_amount = super.convert(builder: container, type: Int.self, forKey: .tax_amount)
            self.iac_amount = super.convert(builder: container, type: Int.self, forKey: .iac_amount)
            self.tip_amount = super.convert(builder: container, type: Int.self, forKey: .tip_amount)
            self.base_amount = super.convert(builder: container, type: Int.self, forKey: .base_amount)
            self.phonenumber = super.convert(builder: container, type: String.self, forKey: .phonenumber)
            self.other_offers = super.convert(builder: container, type: [OtherOfferItem].self, forKey: .other_offers)
            self.redeemedTimes = super.convert(builder: container, type: Int.self, forKey: .redeemedTimes)
            self.coupon_id = super.convert(builder: container, type: String.self, forKey: .coupon_id)
            self.coupons_left = super.convert(builder: container, type: Int.self, forKey: .coupons_left)
            self.downloaded = super.convert(builder: container, type: Bool.self, forKey: .downloaded)
            self.markers = super.convert(builder: container, type: [String].self, forKey: .markers)
            self.pending_payment = super.convert(builder: container, type: [String].self, forKey: .pending_payment)
        }
        
        static public func == (lhs: CardData, rhs: CardData) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
        
        public func custom_decode() -> Void {
            if !self.included!.isEmpty {
                var swap: Bool = false
                self.included_before = []
                self.included_after = []
                for text in self.included! {
                    if text.contains("Cómo redimir tu producto después de hacer el pago") || text.contains("Cómo redimir tu producto después de hacer el pago") {
                        swap = true
                    }
                    if !swap {
                        self.included_before!.append( text )
                    }else {
                        self.included_after!.append( text )
                    }
                }
            }
            if !self.conditions!.isEmpty {
                self.conditions_civico = [self.conditions![0], self.conditions![self.conditions!.count - 1]]
            }
        }
        
        public class OtherOfferItem : Utils, Identifiable, Hashable {
            enum CodingKeys: String, CodingKey, Decodable {
                case id,name,description,price,discount,amount,offer_type,image,date,only_adult,offer_places
            }
            public var id: String?
            public var name: String?
            public var description: String?
            public var price: Double?
            public var discount: Double?
            public var amount: Double?
            public var offer_type: String?
            public var image: String?
            public var date: String?
            public var only_adult: Bool?
            public var offer_places: [OfferPlaceItem]?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.id = super.convert(builder: container, type: String.self, forKey: .id)
                self.name = super.convert(builder: container, type: String.self, forKey: .name)
                self.description = super.convert(builder: container, type: String.self, forKey: .description)
                self.price = super.convert(builder: container, type: Double.self, forKey: .price)
                self.discount = super.convert(builder: container, type: Double.self, forKey: .discount)
                self.amount = super.convert(builder: container, type: Double.self, forKey: .amount)
                self.offer_type = super.convert(builder: container, type: String.self, forKey: .offer_type)
                self.image = super.convert(builder: container, type: String.self, forKey: .image)
                self.date = super.convert(builder: container, type: String.self, forKey: .date)
                self.only_adult = super.convert(builder: container, type: Bool.self, forKey: .only_adult)
                self.offer_places = super.convert(builder: container, type: [OfferPlaceItem].self, forKey: .offer_places)
                
            }
            
            static public func == (lhs: OtherOfferItem, rhs: OtherOfferItem) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
        }
        
        
        public class OfferPlaceItem : Utils, Identifiable, Hashable {
            enum CodingKeys: String, CodingKey, Decodable {
                case id,name,coordinates,address,premium,small_business,phonenumber,schedule
            }
            public var id: String?
            public var name: String?
            public var coordinates: Coordinates?
            public var address: String?
            public var premium: Bool?
            public var small_business: Bool?
            public var phonenumber: String?
            public var schedule: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.id = super.convert(builder: container, type: String.self, forKey: .id)
                self.name = super.convert(builder: container, type: String.self, forKey: .name)
                self.coordinates = super.convert(builder: container, type: Coordinates.self, forKey: .coordinates)
                self.address = super.convert(builder: container, type: String.self, forKey: .address)
                self.premium = super.convert(builder: container, type: Bool.self, forKey: .premium)
                self.small_business = super.convert(builder: container, type: Bool.self, forKey: .small_business)
                self.phonenumber = super.convert(builder: container, type: String.self, forKey: .phonenumber)
                self.schedule = super.convert(builder: container, type: String.self, forKey: .schedule)
                
            }
            
            
            static public func == (lhs: OfferPlaceItem, rhs: OfferPlaceItem) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
        }
        
        public class ScoreItem : Utils, Identifiable, Hashable {
            public let id: UUID = UUID()
            enum CodingKeys: String, CodingKey, Decodable {
                case description,values,average,user
            }
            public var description: String?
            public var values: [Int]?
            public var average: String?
            public var user: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.description = super.convert(builder: container, type: String.self, forKey: .description)
                self.values = super.convert(builder: container, type: [Int].self, forKey: .values)
                self.average = super.convert(builder: container, type: String.self, forKey: .average)
                self.user = super.convert(builder: container, type: String.self, forKey: .user)
                
            }
            
            static public func == (lhs: ScoreItem, rhs: ScoreItem) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
        }
        
    }
}

//------------------------------ COMPLEX FindChipPlaceResponse ------------------------------

public class FindChipPlaceResponse : Utils, Identifiable, Hashable {
    public let id: UUID = UUID()
    enum CodingKeys: String, CodingKey, Decodable {
        case card_data = "card-data"
    }
    
    public var card_data: CardData?
    
    required public init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.card_data = super.convert(builder: container, type: CardData.self, forKey: .card_data)
    }
    
    static public func == (lhs: FindChipPlaceResponse, rhs: FindChipPlaceResponse) -> Bool {
        return lhs.id == rhs.id
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    public class CardData : Utils, Identifiable, Hashable {
        enum CodingKeys: String, CodingKey, Decodable {
            case images_count,schedule_text,schedules,schedule_range,shops,shopping,concessions_count,premium,feature_branding,branding,offers,job_offers,events,badge,allies,source_url,source_alias,taking_place_branding,has_pay,has_subsidiaries,id,card_type,small_business,name,description,address,full_address,neighborhood,parent_neighborhood,reference,email,lat,long,icon,category,features,image,uri,phone_number,phone_numbers,recommended
        }
        public var images_count: Int?
        public var schedule_text: String?
        public var schedules: [String]?
        public var schedule_range: String?
        public var shops: [String]?
        public var shopping: String?
        public var concessions_count: Int?
        public var premium: Bool?
        public var feature_branding: [String]?
        public var branding: BrandingItem?
        public var offers: String?
        public var job_offers: [String]?
        public var events: [String]?
        public var badge: String?
        public var allies: [String]?
        public var source_url: String?
        public var source_alias: String?
        public var taking_place_branding: String?
        public var has_pay: Bool?
        public var has_subsidiaries: Bool?
        public var id: String?
        public var card_type: String?
        public var small_business: Bool?
        public var name: String?
        public var description: String?
        public var address: String?
        public var full_address: String?
        public var neighborhood: String?
        public var parent_neighborhood: String?
        public var reference: String?
        public var email: String?
        public var lat: Double?
        public var long: Double?
        public var icon: String?
        public var category: String?
        public var features: [String]?
        public var image: String?
        public var uri: String?
        public var phone_number: String?
        public var phone_numbers: [String]?
        public var recommended: RecommendedItem?
        
        required public init(from decoder: Decoder) throws {
            super.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.images_count = super.convert(builder: container, type: Int.self, forKey: .images_count)
            self.schedule_text = super.convert(builder: container, type: String.self, forKey: .schedule_text)
            self.schedules = super.convert(builder: container, type: [String].self, forKey: .schedules)
            self.schedule_range = super.convert(builder: container, type: String.self, forKey: .schedule_range)
            self.shops = super.convert(builder: container, type: [String].self, forKey: .shops)
            self.shopping = super.convert(builder: container, type: String.self, forKey: .shopping)
            self.concessions_count = super.convert(builder: container, type: Int.self, forKey: .concessions_count)
            self.premium = super.convert(builder: container, type: Bool.self, forKey: .premium)
            self.feature_branding = super.convert(builder: container, type: [String].self, forKey: .feature_branding)
            self.branding = super.convert(builder: container, type: BrandingItem.self, forKey: .branding)
            self.offers = super.convert(builder: container, type: String.self, forKey: .offers)
            self.job_offers = super.convert(builder: container, type: [String].self, forKey: .job_offers)
            self.events = super.convert(builder: container, type: [String].self, forKey: .events)
            self.badge = super.convert(builder: container, type: String.self, forKey: .badge)
            self.allies = super.convert(builder: container, type: [String].self, forKey: .allies)
            self.source_url = super.convert(builder: container, type: String.self, forKey: .source_url)
            self.source_alias = super.convert(builder: container, type: String.self, forKey: .source_alias)
            self.taking_place_branding = super.convert(builder: container, type: String.self, forKey: .taking_place_branding)
            self.has_pay = super.convert(builder: container, type: Bool.self, forKey: .has_pay)
            self.has_subsidiaries = super.convert(builder: container, type: Bool.self, forKey: .has_subsidiaries)
            self.id = super.convert(builder: container, type: String.self, forKey: .id)
            self.card_type = super.convert(builder: container, type: String.self, forKey: .card_type)
            self.small_business = super.convert(builder: container, type: Bool.self, forKey: .small_business)
            self.name = super.convert(builder: container, type: String.self, forKey: .name)
            self.description = super.convert(builder: container, type: String.self, forKey: .description)
            self.address = super.convert(builder: container, type: String.self, forKey: .address)
            self.full_address = super.convert(builder: container, type: String.self, forKey: .full_address)
            self.neighborhood = super.convert(builder: container, type: String.self, forKey: .neighborhood)
            self.parent_neighborhood = super.convert(builder: container, type: String.self, forKey: .parent_neighborhood)
            self.reference = super.convert(builder: container, type: String.self, forKey: .reference)
            self.email = super.convert(builder: container, type: String.self, forKey: .email)
            self.lat = super.convert(builder: container, type: Double.self, forKey: .lat)
            self.long = super.convert(builder: container, type: Double.self, forKey: .long)
            self.icon = super.convert(builder: container, type: String.self, forKey: .icon)
            self.category = super.convert(builder: container, type: String.self, forKey: .category)
            self.features = super.convert(builder: container, type: [String].self, forKey: .features)
            self.image = super.convert(builder: container, type: String.self, forKey: .image)
            self.uri = super.convert(builder: container, type: String.self, forKey: .uri)
            self.phone_number = super.convert(builder: container, type: String.self, forKey: .phone_number)
            self.phone_numbers = super.convert(builder: container, type: [String].self, forKey: .phone_numbers)
            self.recommended = super.convert(builder: container, type: RecommendedItem.self, forKey: .recommended)
        }
        
        static public func == (lhs: CardData, rhs: CardData) -> Bool {
            return lhs.id == rhs.id
        }
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
        
        public class BrandingItem : Utils, Identifiable, Hashable {
            enum CodingKeys: String, CodingKey, Decodable {
                case id,name,prefix,image,category,feature,template,gradient_color_start,gradient_color_final,tooltip_color,show_map,map_id,branding_feature,event_start_date,event_end_date,header_background_image,header_background_image_app,images,text_color
            }
            public var id: String?
            public var name: String?
            public var prefix: String?
            public var image: String?
            public var category: String?
            public var feature: String?
            public var template: String?
            public var gradient_color_start: String?
            public var gradient_color_final: String?
            public var tooltip_color: String?
            public var show_map: Bool?
            public var map_id: String?
            public var branding_feature: BrandingFeatureItem?
            public var event_start_date: String?
            public var event_end_date: String?
            public var header_background_image: String?
            public var header_background_image_app: String?
            public var images: Picture?
            public var text_color: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.id = super.convert(builder: container, type: String.self, forKey: .id)
                self.name = super.convert(builder: container, type: String.self, forKey: .name)
                self.prefix = super.convert(builder: container, type: String.self, forKey: .prefix)
                self.image = super.convert(builder: container, type: String.self, forKey: .image)
                self.category = super.convert(builder: container, type: String.self, forKey: .category)
                self.feature = super.convert(builder: container, type: String.self, forKey: .feature)
                self.template = super.convert(builder: container, type: String.self, forKey: .template)
                self.gradient_color_start = super.convert(builder: container, type: String.self, forKey: .gradient_color_start)
                self.gradient_color_final = super.convert(builder: container, type: String.self, forKey: .gradient_color_final)
                self.tooltip_color = super.convert(builder: container, type: String.self, forKey: .tooltip_color)
                self.show_map = super.convert(builder: container, type: Bool.self, forKey: .show_map)
                self.map_id = super.convert(builder: container, type: String.self, forKey: .map_id)
                self.branding_feature = super.convert(builder: container, type: BrandingFeatureItem.self, forKey: .branding_feature)
                self.event_start_date = super.convert(builder: container, type: String.self, forKey: .event_start_date)
                self.event_end_date = super.convert(builder: container, type: String.self, forKey: .event_end_date)
                self.header_background_image = super.convert(builder: container, type: String.self, forKey: .header_background_image)
                self.header_background_image_app = super.convert(builder: container, type: String.self, forKey: .header_background_image_app)
                self.images = super.convert(builder: container, type: Picture.self, forKey: .images)
                self.text_color = super.convert(builder: container, type: String.self, forKey: .text_color)
            }
            
            static public func == (lhs: BrandingItem, rhs: BrandingItem) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
            
            public class BrandingFeatureItem : Utils, Identifiable, Hashable {
                enum CodingKeys: String, CodingKey, Decodable {
                    case id,name,logo_image,map_image,search_image,search_text
                }
                public var id: String?
                public var name: String?
                public var logo_image: String?
                public var map_image: String?
                public var search_image: String?
                public var search_text: String?
                
                required public init(from decoder: Decoder) throws {
                    super.init()
                    let container = try decoder.container(keyedBy: CodingKeys.self)
                    self.id = super.convert(builder: container, type: String.self, forKey: .id)
                    self.name = super.convert(builder: container, type: String.self, forKey: .name)
                    self.logo_image = super.convert(builder: container, type: String.self, forKey: .logo_image)
                    self.map_image = super.convert(builder: container, type: String.self, forKey: .map_image)
                    self.search_image = super.convert(builder: container, type: String.self, forKey: .search_image)
                    self.search_text = super.convert(builder: container, type: String.self, forKey: .search_text)
                }
                
                static public func == (lhs: BrandingFeatureItem, rhs: BrandingFeatureItem) -> Bool {
                    return lhs.id == rhs.id
                }
                public func hash(into hasher: inout Hasher) {
                    hasher.combine(id)
                }
            }
        }
        
        public class RecommendedItem : Utils, Identifiable, Hashable {
            enum CodingKeys: String, CodingKey, Decodable {
                case id,card_type,small_business,name,description,address,full_address,neighborhood,parent_neighborhood,categories_tree,reference,email,website,lat,long,icon,category,features,image,image_alternatives,uri,phone_number,phone_numbers,phone_number_decorated,phone_numbers_decorated,phone_numbers_group,user_features,is_theater,movies,is_open,taking_place_branding,schedule,schedules,avatar,info,score,lists,premium,premium_tpl,brand,feature_branding,branding,color
            }
            public var id: String?
            public var card_type: String?
            public var small_business: Bool?
            public var name: String?
            public var description: String?
            public var address: String?
            public var full_address: String?
            public var neighborhood: String?
            public var parent_neighborhood: String?
            public var categories_tree: FirstSecondEIItem?
            public var reference: String?
            public var email: String?
            public var website: String?
            public var lat: Double?
            public var long: Double?
            public var icon: String?
            public var category: String?
            public var features: [String]?
            public var image: String?
            public var image_alternatives: ImageAlternativesItem?
            public var uri: String?
            public var phone_number: String?
            public var phone_numbers: [String]?
            public var phone_number_decorated: PhoneNumbersDecoratedItem?
            public var phone_numbers_decorated: [PhoneNumbersDecoratedItem]?
            public var phone_numbers_group: PhoneNumbersGroupItem?
            public var user_features: [String]?
            public var is_theater: Bool?
            public var movies: [String]?
            public var is_open: Bool?
            public var taking_place_branding: String?
            public var schedule: String?
            public var schedules: [ScheduleItem]?
            public var avatar: String?
            public var info: String?
            public var score: ScoreItem?
            public var lists: [String]?
            public var premium: Bool?
            public var premium_tpl: Int?
            public var brand: BrandItem?
            public var feature_branding: String?
            public var branding: String?
            public var color: String?
            
            required public init(from decoder: Decoder) throws {
                super.init()
                let container = try decoder.container(keyedBy: CodingKeys.self)
                self.id = super.convert(builder: container, type: String.self, forKey: .id)
                self.card_type = super.convert(builder: container, type: String.self, forKey: .card_type)
                self.small_business = super.convert(builder: container, type: Bool.self, forKey: .small_business)
                self.name = super.convert(builder: container, type: String.self, forKey: .name)
                self.description = super.convert(builder: container, type: String.self, forKey: .description)
                self.address = super.convert(builder: container, type: String.self, forKey: .address)
                self.full_address = super.convert(builder: container, type: String.self, forKey: .full_address)
                self.neighborhood = super.convert(builder: container, type: String.self, forKey: .neighborhood)
                self.parent_neighborhood = super.convert(builder: container, type: String.self, forKey: .parent_neighborhood)
                self.categories_tree = super.convert(builder: container, type: FirstSecondEIItem.self, forKey: .categories_tree)
                self.reference = super.convert(builder: container, type: String.self, forKey: .reference)
                self.email = super.convert(builder: container, type: String.self, forKey: .email)
                self.website = super.convert(builder: container, type: String.self, forKey: .website)
                self.lat = super.convert(builder: container, type: Double.self, forKey: .lat)
                self.long = super.convert(builder: container, type: Double.self, forKey: .long)
                self.icon = super.convert(builder: container, type: String.self, forKey: .icon)
                self.category = super.convert(builder: container, type: String.self, forKey: .category)
                self.features = super.convert(builder: container, type: [String].self, forKey: .features)
                self.image = super.convert(builder: container, type: String.self, forKey: .image)
                self.image_alternatives = super.convert(builder: container, type: ImageAlternativesItem.self, forKey: .image_alternatives)
                self.uri = super.convert(builder: container, type: String.self, forKey: .uri)
                self.phone_number = super.convert(builder: container, type: String.self, forKey: .phone_number)
                self.phone_numbers = super.convert(builder: container, type: [String].self, forKey: .phone_numbers)
                self.phone_number_decorated = super.convert(builder: container, type: PhoneNumbersDecoratedItem.self, forKey: .phone_number_decorated)
                self.phone_numbers_decorated = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .phone_numbers_decorated)
                self.phone_numbers_group = super.convert(builder: container, type: PhoneNumbersGroupItem.self, forKey: .phone_numbers_group)
                self.user_features = super.convert(builder: container, type: [String].self, forKey: .user_features)
                self.is_theater = super.convert(builder: container, type: Bool.self, forKey: .is_theater)
                self.movies = super.convert(builder: container, type: [String].self, forKey: .movies)
                self.is_open = super.convert(builder: container, type: Bool.self, forKey: .is_open)
                self.taking_place_branding = super.convert(builder: container, type: String.self, forKey: .taking_place_branding)
                self.schedule = super.convert(builder: container, type: String.self, forKey: .schedule)
                self.schedules = super.convert(builder: container, type: [ScheduleItem].self, forKey: .schedules)
                self.avatar = super.convert(builder: container, type: String.self, forKey: .avatar)
                self.info = super.convert(builder: container, type: String.self, forKey: .info)
                self.score = super.convert(builder: container, type: ScoreItem.self, forKey: .score)
                self.lists = super.convert(builder: container, type: [String].self, forKey: .lists)
                self.premium = super.convert(builder: container, type: Bool.self, forKey: .premium)
                self.premium_tpl = super.convert(builder: container, type: Int.self, forKey: .premium_tpl)
                self.brand = super.convert(builder: container, type: BrandItem.self, forKey: .brand)
                self.feature_branding = super.convert(builder: container, type: String.self, forKey: .feature_branding)
                self.branding = super.convert(builder: container, type: String.self, forKey: .branding)
                self.color = super.convert(builder: container, type: String.self, forKey: .color)
            }
            
            static public func == (lhs: RecommendedItem, rhs: RecommendedItem) -> Bool {
                return lhs.id == rhs.id
            }
            public func hash(into hasher: inout Hasher) {
                hasher.combine(id)
            }
            
            public class ScoreItem : Utils, Identifiable, Hashable {
                public var id: UUID? = UUID()
                enum CodingKeys: String, CodingKey, Decodable {
                    case user
                }
                public var user: Int?
                
                required public init(from decoder: Decoder) throws {
                    super.init()
                    let container = try decoder.container(keyedBy: CodingKeys.self)
                    self.user = super.convert(builder: container, type: Int.self, forKey: .user)
                }
                
                static public func == (lhs: ScoreItem, rhs: ScoreItem) -> Bool {
                    return lhs.id == rhs.id
                }
                public func hash(into hasher: inout Hasher) {
                    hasher.combine(id)
                }
            }
            
            public class BrandItem : Utils, Identifiable, Hashable {
                enum CodingKeys: String, CodingKey, Decodable {
                    case id,name,slug,image,description,premium_tpl,logo,facebook_url,google_url,twitter_url,instagram_url,youtube_url,pinterest_url,linkedin_url,categories,subsidiaries_count,offers_count
                }
                public var id: String?
                public var name: String?
                public var slug: String?
                public var image: String?
                public var description: String?
                public var premium_tpl: Int?
                public var logo: String?
                public var facebook_url: String?
                public var google_url: String?
                public var twitter_url: String?
                public var instagram_url: String?
                public var youtube_url: String?
                public var pinterest_url: String?
                public var linkedin_url: String?
                public var categories: [String]?
                public var subsidiaries_count: String?
                public var offers_count: Int?
                
                required public init(from decoder: Decoder) throws {
                    super.init()
                    let container = try decoder.container(keyedBy: CodingKeys.self)
                    self.id = super.convert(builder: container, type: String.self, forKey: .id)
                    self.name = super.convert(builder: container, type: String.self, forKey: .name)
                    self.slug = super.convert(builder: container, type: String.self, forKey: .slug)
                    self.image = super.convert(builder: container, type: String.self, forKey: .image)
                    self.description = super.convert(builder: container, type: String.self, forKey: .description)
                    self.premium_tpl = super.convert(builder: container, type: Int.self, forKey: .premium_tpl)
                    self.logo = super.convert(builder: container, type: String.self, forKey: .logo)
                    self.facebook_url = super.convert(builder: container, type: String.self, forKey: .facebook_url)
                    self.google_url = super.convert(builder: container, type: String.self, forKey: .google_url)
                    self.twitter_url = super.convert(builder: container, type: String.self, forKey: .twitter_url)
                    self.instagram_url = super.convert(builder: container, type: String.self, forKey: .instagram_url)
                    self.youtube_url = super.convert(builder: container, type: String.self, forKey: .youtube_url)
                    self.pinterest_url = super.convert(builder: container, type: String.self, forKey: .pinterest_url)
                    self.linkedin_url = super.convert(builder: container, type: String.self, forKey: .linkedin_url)
                    self.categories = super.convert(builder: container, type: [String].self, forKey: .categories)
                    self.subsidiaries_count = super.convert(builder: container, type: String.self, forKey: .subsidiaries_count)
                    self.offers_count = super.convert(builder: container, type: Int.self, forKey: .offers_count)
                }
                
                static public func == (lhs: BrandItem, rhs: BrandItem) -> Bool {
                    return lhs.id == rhs.id
                }
                public func hash(into hasher: inout Hasher) {
                    hasher.combine(id)
                }
            }
            
            public class PhoneNumbersDecoratedItem : Utils, Identifiable, Hashable {
                enum CodingKeys: String, CodingKey, Decodable {
                    case id,type,number,number_e164,mobile_dialing
                }
                public var id: String?
                public var type: String?
                public var number: String?
                public var number_e164: String?
                public var mobile_dialing: String?
                
                required public init(from decoder: Decoder) throws {
                    super.init()
                    let container = try decoder.container(keyedBy: CodingKeys.self)
                    self.id = super.convert(builder: container, type: String.self, forKey: .id)
                    self.type = super.convert(builder: container, type: String.self, forKey: .type)
                    self.number = super.convert(builder: container, type: String.self, forKey: .number)
                    self.number_e164 = super.convert(builder: container, type: String.self, forKey: .number_e164)
                    self.mobile_dialing = super.convert(builder: container, type: String.self, forKey: .mobile_dialing)
                }
                
                static public func == (lhs: PhoneNumbersDecoratedItem, rhs: PhoneNumbersDecoratedItem) -> Bool {
                    return lhs.id == rhs.id
                }
                public func hash(into hasher: inout Hasher) {
                    hasher.combine(id)
                }
            }
            
            public class PhoneNumbersGroupItem : Utils, Identifiable, Hashable {
                public var id: UUID? = UUID()
                enum CodingKeys: String, CodingKey, Decodable {
                    case fixed,mobile,fax,beeper,hotline,clientservice,domicile,whatsapp,contact,other
                }
                public var fixed: [PhoneNumbersDecoratedItem]?
                public var mobile: [PhoneNumbersDecoratedItem]?
                public var fax: [PhoneNumbersDecoratedItem]?
                public var beeper: [PhoneNumbersDecoratedItem]?
                public var hotline: [PhoneNumbersDecoratedItem]?
                public var clientservice: [PhoneNumbersDecoratedItem]?
                public var domicile: [PhoneNumbersDecoratedItem]?
                public var whatsapp: [PhoneNumbersDecoratedItem]?
                public var contact: [PhoneNumbersDecoratedItem]?
                public var other: [PhoneNumbersDecoratedItem]?
                
                required public init(from decoder: Decoder) throws {
                    super.init()
                    let container = try decoder.container(keyedBy: CodingKeys.self)
                    self.fixed = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .fixed)
                    self.mobile = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .mobile)
                    self.fax = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .fax)
                    self.beeper = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .beeper)
                    self.hotline = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .hotline)
                    self.clientservice = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .clientservice)
                    self.domicile = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .domicile)
                    self.whatsapp = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .whatsapp)
                    self.contact = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .contact)
                    self.other = super.convert(builder: container, type: [PhoneNumbersDecoratedItem].self, forKey: .other)
                }
                
                static public func == (lhs: PhoneNumbersGroupItem, rhs: PhoneNumbersGroupItem) -> Bool {
                    return lhs.id == rhs.id
                }
                public func hash(into hasher: inout Hasher) {
                    hasher.combine(id)
                }
            }
            
        }
        
    }
}

struct DataResponseType_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
