import Foundation

public class MessageRequest : Encodable {
    public var work_at_place: WorkAtPlace?
    public var id: String?
    init() { }
    public init(id: String) {
        self.id = id
    }
    public init(place: WorkAtPlace) {
        self.work_at_place = place
    }
    public init(place: WorkAtPlace, id: String) {
        self.work_at_place = place
        self.id = id
    }
}

public class WorkAtPlace : Encodable {
    public var type: String?
    public var name: String?
    public var email: String?
    public var phone: String?
    public var message: String?
    public init() {}
    public init(type: String, email: String, phone: String){
        self.type = type
        self.email = email
        self.phone = phone
    }
}
