import UIKit
import SwiftUI
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

// ------------------------------------------ CIVICO CONSUME ------------------------------------------

//--------------------------- Buscador Lugares - GET --------------------------
//var endpoint: String = "https://www.civico.com/api/v1/search/query/places"
//var params: [String:String] = ["term":"dulces",
//                               "lat":"4.755786",
//                               "lng":"-74.0417624",
//                               "radio":"1",
//                               "user_lat":"4.755786",
//                               "user_lng":"-74.0417624",
//                               "page":"1",
//                               "limit":"10",
//                               "rate":"5" ,
//                               "facet": "corrientazos,regalos-y-sorpresas",
//                               "open":"true",
//                               "features":"[bebidas-energizantes]"]

//var params: [String:String] = ["term": "Desinfectante ",
//                               "limit": "10",
//                               "radio": "1",
//                               "open": "true",
//                               "lng": "-74.0417624",
//                               "user_lng": "-74.0417624",
//                               "user_lat": "4.755786",
//                               "page": "1",
//                               "rate": "",
//                               "features": "",
//                               "lat": "4.755786",
//                               "facet": ""]
//let client = ClientRestFul< FindResponse >(endpoint: endpoint, debug: false)
//    .params(params:params)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: FindResponse = response.data!
//            print("isOpen: \(data.isOpen)")
//            //            print("filters: \(data.filters?.count)")
//            print("results: \(data.results?.count ?? 0)")
//            for item in data.results! {
//                //                print("name: \(item.name)")
//                print("phone_number_decorated: \(item.phone_number_decorated)")
//            }
//        } else {
//            print("message:\(response.message)")
//        }
//    }

//--------------------------- Buscador Productos - GET --------------------------
let endpoint: String = "https://www.civico.com/api/v1/search/query/offers"
//let endpoint: String = "https://www.civico.com/api/v1/search/query/places"
//let params: [String:String] = ["term":"perros",
//                               "lat":"4.755786",
//                               "lng":"-74.0417624",
//                               "user_lat":"4.755786",
//                               "user_lng":"-74.0417624",
//                               "page":"1",
//                               "limit":"10",
//                               "radio":"10" ,
//                               "category": "mascotas",
//                               "include_transactional":"true"]

//let params: [String:String] = ["radio": "10",
//                               "include_transactional": "true",
//                               "user_lat": "4.755786",
//                               "limit": "30",
//                               "page": "1",
//                               "category": "",
//                               "term": "Desinfectante",
//                               "user_lng": "-74.0417624",
//                               "lat": "4.755786",
//                               "lng": "-74.0417624"]

//let params: [String:String] = [    "term": "Desinfectante",
//                                   "lat":"4.755786",
//                                   "lng":"-74.0417624",
//                                   "radio":"1",
//                                   "user_lat":"4.755786",
//                                   "user_lng":"-74.0417624",
//                                   "page":"1",
//                                   "limit":"30",
//                                   "rate":"",
//                                   "facet": "",
//                                   "open":"true",
//                                   "features":""]

let params: [String:String] = [    "term": "Desinfectante",
                                   "lat":"4.755786",
                                   "lng":"-74.0417624",
                                   "user_lat":"4.755786",
                                   "user_lng":"-74.0417624",
                                   "page":"1",
                                   "limit":"30",
                                   "radio":"10" ,
                                   "category": "",
                                   "include_transactional":"true"]

let client = ClientRestFul< FindResponse >(endpoint: endpoint, debug: false )
    .params(params:params)
    .get()
    .commit { (response) in
        print("status:\(response.success)")
        if response.success {
            let data: FindResponse = response.data!
            print("isOpen: \(data.isOpen)")
            print("from: \(data.from)")
            print("description: \(data.copies?.offer?.normalPrice?.description)")
            print("closed: \(data.copies?.delivery?.closed)")
            //            print("isOpen: \(data.isOpen)")
            //            print("uuid: \(data.copies?.delivery?.open)")
            //            print("data: \(data.results?.count ?? 0)")
            for item in data.results! {
                
                print("coordinates: \(item.coordinates?.lat)")
                //                Text("coordinates: \(item.coordinates!.latitude!)")
                
                print("price: \(item.price)")
                //                Text("price: \(item.price!)")
                
                print("number_e164: \(item.phone_number_decorated?.number_e164)")
                //                Text("number_e164: \(String(item.phone_number_decorated!.number_e164!))")
                
                //                print("price: \(item.id_response)")
                //                print("is_open: \(item.is_open)")
                //                let name: String? = AnyType.get(t: item.name)
                //                print("name: \(name)")
                //                let nn: String? = AnyType.get(t: item.phone_number_decorated)?.number
                //                print("phone_number_decorated: \(item.phone_number_decorated)")
                //                print("phone_number_decorated.number: \(AnyType.get(t: item.phone_number_decorated)?.number)")
                
                //                let nn: FindResponse.ResultItem.ResultItemPhoneNumberDecorated? = AnyType.get(t: item.phone_number_decorated)
                
                //                let num: String? = AnyType.get(t: item.phone_number_decorated?.number_e164)
                //                Text("any_type.phone_number_decorated: \(AnyType.get(t: nn?.number_e164)!)")
                
            }
            //print("data: \(self.resultProducts.from!)")
            //print("data: \(self.resultPlaces.error_location!)")
            //print("data: \(self.resultPlaces.copies!.delivery!)")
        } else {
            print("message:\(response.message)")
        }
    }

//--------------------------- Autocomplete Lugares - GET --------------------------
//var endpoint: String = "https://www.civico.com/api/v2/search/entities"
//var params: [String:String] = ["entities[]":"Place",
//                               "term": "perro",
//                               "lat":"4.7646443",
//                               "lng":"-74.0491937" ]
//let client = ClientRestFul< AutocompleteResponse >(endpoint: endpoint)
//    .params(params:params)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: AutocompleteResponse = response.data!
//            print("data: \(data.entities!)")
//        } else {
//            print("message:\(response.message)")
//        }
//}


//--------------------------- Autocomplete Productos Lugares - GET --------------------------
//var endpoint: String = "https://www.civico.com/api/v2/search/entities"
//var params: [String:String] = ["entities[]":"Offer",
//                               "term": "perro",
//                               "lat":"4.7646443",
//                               "lng":"-74.0491937" ]
//let client = ClientRestFul< AutocompleteResponse >(endpoint: endpoint)
//    .params(params:params)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: AutocompleteResponse = response.data!
//            print("data: \(data.entities!)")
//        } else {
//            print("message:\(response.message)")
//        }
//}


//--------------------------- Listar mis mensajes  --------------------------
//let endpointFindAllMessages: String = "https://beta.civico.com/api/v1/users/commerce_messages"
//let params: [String:String] = ["auth_token":"2799f0f0ff56813eee7875350ee6556a7c32ea69083631576480e6338546652d" ]
//
//let client = ClientRestFul< FindMessageResponse >(endpoint: endpointFindAllMessages)
//    .params(params:params)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: FindMessageResponse = response.data!
//            print("results: \(data.results)")
//            print("metadata: \(data.metadata)")
//
//        } else {
//            print("message:\(response.message)")
//        }
//}

//--------------------------- Consultar un mensaje - GET --------------------------
//let endpointFindByIdMessage: String = "https://beta.civico.com/api/v1/users/commerce_messages/5f2c61f69496f94aad000002"
//let params: [String:String] = ["auth_token":"2799f0f0ff56813eee7875350ee6556a7c32ea69083631576480e6338546652d" ]
//
//let client = ClientRestFul< FindMessageResponse >(endpoint: endpointFindByIdMessage)
//    .params(params:params)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: FindMessageResponse = response.data!
//            print("message: \(data.message)")
//            print("responses: \(data.responses)")
//        } else {
//            print("message:\(response.message)")
//        }
//}

//--------------------------- Crear Mensaje al comercio - POST --------------------------
//let endpointCreateMessage: String = "https://beta.civico.com/api/v2/places/52e6dbd331e93ca0c70000bb/worker"
//let headers: [String:String] = ["Authorization":"Bearer 2799f0f0ff56813eee7875350ee6556a7c32ea69083631576480e6338546652d",
//                                "Content-Type": "application/json"]
//
//var workAtPlace = WorkAtPlace(type: "Cotizaciones", email: "luiscarlos@civico.com", phone: "3172840228")
//workAtPlace.name = "Luis Villamil"
//workAtPlace.message = "a como las vagas?"
//var messageRequest = MessageRequest(place: workAtPlace, id:"52e6dbd331e93ca0c70000bb")
//
//let client = ClientRestFul< StatusResponse >(endpoint: endpointCreateMessage)
//    .headers(params: headers)
//    .post(body: messageRequest)
//    .commit{ (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: StatusResponse = response.data!
//            print("data: \(data)")
//            print("data: \(data.status)")
//        }
//}

//--------------------------- Crear una respuesta - POST --------------------------
//let endpointCreateAnswer: String = "https://beta.civico.com/api/v1/users/commerce_messages/5f2c61f69496f94aad000002/response"
//let params: [String:String] = ["auth_token":"2799f0f0ff56813eee7875350ee6556a7c32ea69083631576480e6338546652d",
//                               "text":"Se estan haciendo los locos o que"]
//let headers: [String:String] = ["Content-Type": "application/json"]
//let client = ClientRestFul< StatusResponse >(endpoint: endpointCreateAnswer)
//    .params(params: params)
//    .headers(params: headers)
//    .post()
//    .commit{ (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: StatusResponse = response.data!
//            print("data: \(data)")
//            print("data: \(data.success)")
//        }
//    }

//--------------------------- Marcar como leido - PATCH --------------------------

//let endpointMarkRead: String = "https://beta.civico.com/api/v1/users/commerce_messages/5f2c61f69496f94aad000002"
//let params: [String:String] = ["auth_token":"2799f0f0ff56813eee7875350ee6556a7c32ea69083631576480e6338546652d",
//                               "status": "read"]
//let client = ClientRestFul< StatusResponse >(endpoint: endpointMarkRead)
//    .params(params: params)
//    .patch()
//    .commit{ (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: StatusResponse = response.data!
//            print("data: \(data)")
//        }
//}

//--------------------------- Obtener arbol de categorias - GET --------------------------
//let endpoint: String = "https://beta.civico.com/api/v1/categories_tree"
//let params: [String:String] = ["tree_type":"Place",
//                               "id": "50fec1a731e93cc84100005c"]
//
//let client = ClientRestFul< FindCategoryResponse >(endpoint: endpoint)
//    .params(params:params)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: FindCategoryResponse = response.data!
//            print("message: \(data.categories?[0])")
//        } else {
//            print("message:\(response.message)")
//        }
//}

//--------------------------- Ficha de producto - GET --------------------------
//let endpoint: String = "https://www.civico.com/api/v1/cards-data/offers/5f07ad478d2c626e4c00001a"
//let client = ClientRestFul< FindChipProductResponse >(endpoint: endpoint)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: FindChipProductResponse = response.data!
//            data.card_data?.custom_decode()
//            print("message: \(data.card_data?.conditions_civico)")
//            print("message: \(data.card_data?.included_before)")
//            print("message: \(data.card_data?.included_after)")
//        } else {
//            print("message:\(response.message)")
//        }
//}

//--------------------------- Ficha de lugar - GET --------------------------
//let endpoint: String = "https://beta.civico.com/api/v1/cards-data/places/524b243231e93cb36200002e"
//let client = ClientRestFul< FindChipPlaceResponse >(endpoint: endpoint)
//    .get()
//    .commit { (response) in
//        print("status:\(response.success)")
//        if response.success {
//            let data: FindChipPlaceResponse = response.data!
//            print("message: \(data.card_data)")
//        } else {
//            print("message:\(response.message)")
//        }
//}

