//
//  ContentView.swift
//  Shared
//
//  Created by Esteban Moncaleano on 17/08/20.
//ˇˇˇ

import SwiftUI

struct ContentView: View {
    
    @State var resultProducts: FindResponse = FindResponse()
    
    var body: some View {
        VStack {
            ScrollView {
                VStack {
                    if resultProducts.results != nil {
                        Text("---- begin ----")
                        Text("normalPrice:\(resultProducts.copies!.offer!.normalPrice!) ").padding()
                        ForEach (resultProducts.results!, id: \.self) { item  in
                            Text("name:\(item.name!) ").padding()
                            Text("price:\(item.price!) ").padding()
//                            Text("number_e164:\(item.phone_number_decorated!.number_e164!) ").padding()
                        }
                        Text("---- end ----")
                    } else {
                        Text("Cargandooooo...")
                    }
                }.onAppear(){
                    let endpoint: String = "https://www.civico.com/api/v1/search/query/offers"
                    //var params: [String:String] = ["term":"perros",
                    //                               "lat":"4.755786",
                    //                               "lng":"-74.0417624",
                    //                               "user_lat":"4.755786",
                    //                               "user_lng":"-74.0417624",
                    //                               "page":"1",
                    //                               "limit":"10",
                    //                               "radio":"10" ,
                    //                               "category": "mascotas",
                    //                               "include_transactional":"true"]

                    let params: [String:String] = ["radio": "10",
                                                   "include_transactional": "true",
                                                   "user_lat": "4.755786",
                                                   "limit": "30",
                                                   "page": "1",
                                                   "category": "",
                                                   "term": "Desinfectante",
                                                   "user_lng": "-74.0417624",
                                                   "lat": "4.755786",
                                                   "lng": "-74.0417624"]

                    let client = ClientRestFul<FindResponse>(endpoint: endpoint, debug: false)
                        .params(params:params)
                        .get()
                        .commit { (response) in
                            print("endpoint:\(endpoint)")
                            print("params:\(params)")
                            print("status:\(response.success)")
                            if response.success {
                                self.resultProducts = response.data!
                                print("data: \(self.resultProducts.results?.count ?? 0)")
                                //print("data: \(self.resultProducts.from!)")
                                //print("data: \(self.resultProducts.error_location!)")
                                //print("data: \(self.resultProducts.copies!.delivery!)")
                            } else {
                                print("message: \(response.message)")
                            }
                        }
                }
            }
        }
    }

    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            ContentView()
        }
    }
    
}
