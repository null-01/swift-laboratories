//
//  ClientRestFul.swift
//  swift-code
//
//  Created by ANDRES on 7/08/20.
//  Copyright © 2020 andresduran0205. All rights reserved.
//

import Foundation

extension String {
    func stringByAddingPercentEncodingForRFC3986() -> String? {
        let unreserved = "-._~/?"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        return addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
    }
}

public class ClientRestFul<T : Decodable> {
    private var endpoint: String
    private var request: URLRequest?
    private var outcome: Response<T>?
    
    private var debug: Bool
    private var buildError: Bool = false
    
    private func initialize(){
        guard let URI = URL(string: self.endpoint) else { return }
        self.request = URLRequest(url: URI)
    }
    
    public init(endpoint: String, debug: Bool = false){
        self.endpoint =  endpoint
        self.debug = debug
        initialize()
    }
    
    @discardableResult
    public func params(params: [String:String]) -> ClientRestFul<T> {
        var first: Bool = true
        for(key, value) in params {
            self.endpoint += (first ? "?":"&")
            self.endpoint += "\(key)=\(value.stringByAddingPercentEncodingForRFC3986()!)"
            first = false
        }
        
        self.request?.url = URL(string: self.endpoint)
        return self
    }
    
    @discardableResult
    public func headers(params: [String:String]) -> ClientRestFul<T> {
        for(key, value) in params {
            self.request?.addValue(value, forHTTPHeaderField: key)
        }
        return self
    }
    
    @discardableResult
    public func get() -> ClientRestFul<T> {
        self.request?.httpMethod = "GET"
        return self
    }
    
    @discardableResult
    public func post() -> ClientRestFul<T>  {
        self.request?.httpMethod = "POST"
        return self
    }
    
    @discardableResult
    public func post<U : Encodable>(body: U) -> ClientRestFul<T>  {
        self.request?.httpMethod = "POST"
        do{
            let data = try JSONEncoder().encode(body)
            self.request?.httpBody = data
        } catch let error as NSError{
            if self.debug {
                print("json-handling-encode-error: \(error)")
            }
            self.outcome = Response(success: false, message: error.localizedDescription)
            self.buildError = true
        }
        return self
    }
    
    @discardableResult
       public func patch() -> ClientRestFul<T>  {
           self.request?.httpMethod = "PATCH"
           return self
       }
    
    public func commit(completion: @escaping ( (Response<T>) -> ())) -> ClientRestFul<T> {
        
        let task = URLSession.shared.dataTask(with: self.request!){ (data, response, error) in
            
            if self.buildError {
                self.dispatch(response: self.outcome, completion: completion)
                return
            }
            
            guard let data = data, error == nil else {
                if self.debug {
                    print("response-failed-data: \(error!)")
                }
                self.outcome = Response(success: false, message: "\(error!)" )
                self.dispatch(response: self.outcome, completion: completion)
                return
            }
            
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                if self.debug {
                    print("response-failed: \(response)")
                }
                self.outcome = Response(success: false, message: "\(response!)")
                return
            }
            if let body_response = String(data: data, encoding: String.Encoding.utf8) {
                if self.debug {
                    print("response-success: \(body_response)")
                }
            }
            
            
            do{
                let data = try JSONDecoder().decode(T.self, from: data)
                self.outcome = Response(data)
            } catch let error as NSError{
                if self.debug {
                    print("json-handling-decode-error: \(error)")
                }
                self.outcome = Response(success: false, message: error.localizedDescription)
            }
            self.dispatch(response: self.outcome, completion: completion)
        }
        task.resume()
        return self
    }
    
    private func dispatch(response: Response<T>?, completion: @escaping ( (Response<T>) -> ())){
        DispatchQueue.main.async {
            completion(response!)
        }
    }
    
}
